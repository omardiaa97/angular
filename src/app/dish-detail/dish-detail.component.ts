import { Component, OnInit, Inject } from '@angular/core';
import {DatePipe} from '@angular/common';
import { Params, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Dish } from '../shared/dish';
import { DISHES} from '../shared/dishes';
import { DishService } from '../services/dish.service';
import {switchMap} from 'rxjs/operators';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-dish-detail',
  templateUrl: './dish-detail.component.html',
  styleUrls: ['./dish-detail.component.scss']
})



export class DishDetailComponent implements OnInit {
  dish: Dish;
  reviews: Dish;
  dishCopy = null;
  dishIds: number[];
  prev: number;
  next: number;
  review: Dish['comments'];
  reviewForm: FormGroup;
  name: string;
  comment: string;
  rating: number;
  errMess: string;
  reviewFormErrors = {
    'name': '',
    'comment': '',
    'author': '',
    'date': ''
  };

  reviewValidationMessages = {
    'name': {
      'minlength': 'Name must contain more than 2 characters',
      'maxlength': 'Name cannot contain more than 25 characters',
      'required': 'Name is required!' },
    'comment': {'required': 'Comment is required!'}
  }
  constructor(private datePipe: DatePipe,
              private dishservice: DishService,
              private route: ActivatedRoute,
              private location: Location,
              private fb: FormBuilder,
              @Inject ('BaseURL') private BaseURL) {this.createReviewForm(); }

  ngOnInit() {
    this.dishservice.getDishIds().subscribe(dishIds => this.dishIds = dishIds);
    this.route.params.pipe(switchMap((params: Params) => this.dishservice.getDish(+params['id']))).subscribe(dish => {
      this.dish = dish;
      this.dishCopy = dish;
      this.setPrevNext(dish.id);
    });
    this.dishservice.getDishIds().subscribe(dishIds => this.dishIds = dishIds);
  }

  createReviewForm() {
    this.reviewForm = this.fb.group({
      'name': ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25)]],
      'comment': ['', [Validators.required]],
      'rating': 5
    });
    this.reviewForm.valueChanges.subscribe( data => this.onValueChanges(data));
    this.onValueChanges();
  }

  setPrevNext(dishId: number) {
    let index = this.dishIds.indexOf(dishId);
    this.prev = this.dishIds[(this.dishIds.length + index - 1) % this.dishIds.length];
    this.next = this.dishIds[(this.dishIds.length + index + 1) % this.dishIds.length];

  }


  onValueChanges(data?: any) {
    if (!this.reviewForm) {return;}
    const form = this.reviewForm;
    for (const field in this.reviewFormErrors) {
      this.reviewFormErrors[field] = '';
      const reviewControl = form.get(field);
      if ( reviewControl && reviewControl.dirty && reviewControl.invalid){
        const messages = this.reviewValidationMessages[field];
        for ( const key in reviewControl.errors) {
          this.reviewFormErrors[field] += messages[key] + ' ';
        }
      }
    }
  }

  goBack(): void {
    this.location.back();
  }
  onSubmit() {
    this.review = this.reviewForm.value;

    this.review. = new Date().toISOString();
    const liveComment = {
       'rating' : this.rating,
      'comment': this.comment,
      'name': this.name,
    };
    this.dishCopy.comments.push(this.comment);
    this.dishCopy.save().subscribe(dish => this.dish = dish);
    console.log('Comment added!');
    this.reviewForm.reset({
      name: '',
      comment: '',
      rating: 5
    });
  }
}
