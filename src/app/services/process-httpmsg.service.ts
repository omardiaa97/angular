import { Injectable } from '@angular/core';
import {Observable, throwError} from 'rxjs';
import {Http, Response} from '@angular/http';
import {HttpErrorResponse} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProcessHTTPMsgService {

  constructor() { }

  public extractData(res: Response) {
    let body = res.json();

    return body || { };
}
  public handlingError(error: HttpErrorResponse | any) {
    let errMsg: string;
    if (error.error instanceof ErrorEvent) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = error.error.message;
    }
      else {
        errMsg = `${error.status} - ${error.statusText || ''} ${error.error}`;
    }
    return throwError(errMsg);
  }
}
