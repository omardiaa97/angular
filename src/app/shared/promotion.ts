export class Promotion {
  id: number;
  name: string;
  image: string;
  featured: boolean;
  label: string;
  price: string;
  description: string;

}
